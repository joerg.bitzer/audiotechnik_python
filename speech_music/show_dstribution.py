# This file shows the histogram of speech and music and two different 
# fittings (Gauss and LaPlace).
# public domain: 
# comments to: J. Bitzer , TGM, Jade Hochschule
# Version 0.1 first build 

import numpy as np
import soundfile as sf

import matplotlib.pyplot as plt


speech_filename = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/speech_music/Russisch.wav'
music_filename = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/speech_music/DireStraitsShort6.wav'


speech_signal, fs = sf.read(speech_filename)
music_signal, fs = sf.read(music_filename)

mean_speech = np.mean(speech_signal)
mean_music = np.mean(music_signal)
var_speech = np.var(speech_signal)
var_music = np.var(music_signal)

hist_vals_speech, speech_bins = np.histogram(speech_signal,100, density=True)
hist_vals_music, music_bins = np.histogram(music_signal,100, density=True)


# theoretical densities for given parameters
x_vals = np.linspace(-1,1,num=200)

# gaus fit
y_speech = 1/(np.sqrt(2*np.pi)*np.sqrt(var_speech))*np.exp(-0.5*((x_vals-mean_speech)**2/var_speech))
y_music = 1/(np.sqrt(2*np.pi)*np.sqrt(var_music))*np.exp(-0.5*((x_vals-mean_music)**2/var_music))

# laplace
y_speech_laplace = np.exp(-np.abs(x_vals-mean_speech)/np.sqrt(var_speech))/(2*np.sqrt(var_speech))
y_music_laplace = np.exp(-np.abs(x_vals-mean_music)/np.sqrt(var_music))/(2*np.sqrt(var_music))

# plot results
fig, ax = plt.subplots(ncols=2)

# plot data as bar graph
ax[0].bar(speech_bins[:-1],hist_vals_speech,0.01)
ax[0].plot(x_vals,y_speech)
ax[0].plot(x_vals,y_speech_laplace)


ax[1].bar(music_bins[:-1],hist_vals_music,0.01)
ax[1].plot(x_vals,y_music)
ax[1].plot(x_vals,y_music_laplace)

plt.show()

