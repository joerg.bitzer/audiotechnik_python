# This file contains functions to compute different aspects of dynamic processors
#
# comments to: J. Bitzer , TGM, Jade Hochschule
# Version 0.1 static characteristic curve of compressors

# todo:
# expander
# dynamics 
# full dynamic processor


import numpy as np
import scipy.signal as sig
import soundfile as sf
import matplotlib.pyplot as plt

def get_compression_gain(indata, Threshold,Ratio,DesiredGain = None,bDrawInputOutput = False):

    if DesiredGain is None:
        DesiredGain = Threshold*(1/Ratio-1)


    out_gain = np.zeros_like(indata)

    indata_log = 10*np.log10(indata)

    out_gain[indata_log<=Threshold] = 10**(DesiredGain/20)
    #out_gain[indata_log>Threshold] = indata_log[indata_log>Threshold]*(1/Ratio-1) # works only with (0,0) and auto-makeup
    out_gain[indata_log>Threshold] = DesiredGain - (indata_log[indata_log>Threshold] - Threshold) * (1-1/Ratio)
    out_gain[indata_log>Threshold] = 10**(out_gain[indata_log>Threshold]/20)

    if bDrawInputOutput:
        fig,ax = plt.subplots(ncols = 2)
        ax[0].plot(10*np.log10(indata),20*np.log10(out_gain))
        ax[0].set_title('Gain vs input power')
        ax[0].axis('equal')

        ax[1].plot(10*np.log10(indata),20*np.log10(out_gain)+10*np.log10(indata))
        ax[1].set_title('input vs. output power')
        ax[1].axis('square')
        ax[1].set_xlim([-50.2,0.2])
        ax[1].set_ylim([-50.2,0.2])

        plt.show()

    return out_gain

if __name__ == '__main__':
    x = np.logspace(-5,0,num=101)
    T = -30
    G = -10
    R = 8

    gain = get_compression_gain(x,T,R,G,True)


