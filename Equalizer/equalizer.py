# This file contains functions to design typical biquad (SOS) equalizer filter
# all designs are based on the RBJ cookbook by Robert Bristow-Johnson
# https://webaudio.github.io/Audio-EQ-Cookbook/audio-eq-cookbook.html
# or
# Orfanidis, Sophoncles J., "Digital Parametric Equalizer Design with Prescribed Nyquist-Frequency Gain",
# JAES Volume 45 Issue 6 pp. 444-455; June 1997, https://www.aes.org/e-lib/browse.cfm?elib=13397
# The coefficients are designed to be used for functions like scipy.signal.lfilter or freqz
# The underlying system model with a0 = 1 (differential equation is)
#   y(k) = b0*x(k) + b1*x(k-1) + b2*x(k-2) - a1*y(k-1) - a2*y(k-2)
#
# comments to: J. Bitzer , TGM, Jade Hochschule
# Version 0.1 first build
# version 0.2 RBJ
# version 0.3 Orfanidis Peak, 10.10.2022, JB

# todo:
# analog designs for optimized q Orfanidis
# analog designs as prototypes for FDLS design


import numpy as np
import scipy.signal as sig
import soundfile as sf
import matplotlib.pyplot as plt


def compute_analog_butterworth(order, C=1):
    """This function computes the numerator = 1 and denominator of the analog system function H(s) = C(s)/D(s)
    for an analog butterworth prototype
    Input: 
        order: the order of the desired filter (integer)
        C: design paramter for the attanuation at the normalized frequency 1 (default C = 1 ≙ -3 dB)
    returns: d,c numerator d and denominator c of the transfer function polynomials in s
    example: order = 2, C = 1 ==> d = [1 0 0], c = [1 sqrt(2) 1] """

    s_poly = [1]
    for count in range(2*order-1):
        s_poly.append(0)

    s_poly.append((-1)**order*C**2)
    s_poly = np.array(s_poly)

    roots_circle = np.roots(s_poly)
    print(roots_circle)
    left_roots = roots_circle[np.real(roots_circle) <= 0]
    print(left_roots)

    system_func_den = np.poly(left_roots)
    print(system_func_den)
    d = 1
    c = system_func_den
    return d, c


def bandwidth_to_Q(bandwidth, normalized_omega):
    '''function to convert Bandwidth (BW) in Octaves to Q values
    Parameter: bandwidth: the desired bandwidth in octaves
    normalized_omega: the design frequency normalized between 0 and 2pi (max should be pi)

    returns: Q factor for the design of the equalizer filter
    '''
    Q = 1/(2*np.sinh(np.log(2)/2*bandwidth *
           normalized_omega/np.sin(normalized_omega)))
    return Q


def Q_to_bandwidth(Q, normalized_omega):
    '''function to convert Q in Bandwidth (digital)
    Parameter: Q: the desired Q factor
    normalized_omega: the design frequency normalized between 0 and 2pi (max should be pi)

    returns: Bandwidth in Octaves'''

    BW = np.arcsinh(1/(2*Q))/(np.log(2)/2 *
                              normalized_omega/np.sin(normalized_omega))
    return BW


def Q_to_bandwidth_analog(Q):
    '''function to convert Q in Bandwidth (analog)
    Parameter: Q: the desired Q factor

    returns: Bandwidth in Octaves'''
    BW = np.arcsinh(1/(2*Q))/(np.log(2)/2)
    return BW

# def slope_to_Q():
#    '''function to convert slope (for shelving filters) to Q values'''
#    BW = 0
#    return BW


def compute_alpha(normalized_omega, Q=None, bandwidth=None):
    '''function to compute the alpha helping variable in the RBJ cookbook
    normalized_omega: the design frequency normalized between 0 and 2pi (max should be pi)
    Parameter: Q or bandwidth: the desired Q or bandwidth 

    returns alpha: a helping variable for the design
    '''

    if bandwidth is not None:
        Q = bandwidth_to_Q(bandwidth, normalized_omega)

    if Q is not None:
        alpha = np.sin(normalized_omega)/(2*Q)

    return alpha


def compute_normalized_omega(design_frequency_Hz, sampling_rate_Hz):
    '''function to normalize frequencies from Hz to rad
    Parameter: design_frequency_Hz: the desired cutoff/peak/slope frequency
    sampling_rate_Hz: the sampling rate
    returns the normalized frequency in radian range (0...2pi)'''
    return 2*np.pi*design_frequency_Hz/sampling_rate_Hz


def compute_lowpass_second_order(design_frequency_Hz, sampling_rate_Hz, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section lowpass filter
    Parameter: design_frequency_Hz: Cutoff frequency (-3dB point) in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    Q: the Q factor, Q = 1/sqrt(2) leads to a maximally flat (Butterworth)-design
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)

    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha
    b = []
    b.append((1-cos_omega)*0.5/a0)
    b.append((1-cos_omega)/a0)
    b.append((1-cos_omega)*0.5/a0)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha)/a0)

    return b, a


def compute_highpass_second_order(design_frequency_Hz, sampling_rate_Hz, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section highpass filter
    Parameter: design_frequency_Hz: Cutoff frequency (-3dB point) in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    Q: the Q factor, Q = 1/sqrt(2) leads to a maximally flat (Butterworth)-design
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)

    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha
    b = []
    b.append((1+cos_omega)*0.5/a0)
    b.append(-(1+cos_omega)/a0)
    b.append((1+cos_omega)*0.5/a0)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha)/a0)

    return b, a


def compute_bandpass_second_order(design_frequency_Hz, sampling_rate_Hz, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section bandpass filter
    Parameter: design_frequency_Hz: Mid frequency in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    Q: the Q factor, Q = 1/sqrt(2) leads to a maximally flat (Butterworth)-design
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''

    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)

    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha
    b = []
    b.append(alpha/a0)
    b.append(0)
    b.append(-alpha/a0)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha)/a0)

    return b, a


def compute_notch_second_order(design_frequency_Hz, sampling_rate_Hz, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section notch filter
    Parameter: design_frequency_Hz: Notch frequency (-inf dB point) in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    Q: the Q factor, 
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''

    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)

    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha
    b = []
    b.append(1/a0)
    b.append(-2*cos_omega/a0)
    b.append(1/a0)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha)/a0)

    return b, a


def compute_allpass_second_order(design_frequency_Hz, sampling_rate_Hz, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section allpass filter
    Parameter: design_frequency_Hz: Phase turning frequency (pi) in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    Q: the Q factor, 
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)

    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha
    b = []
    b.append((1-alpha)/a0)
    b.append(-2*cos_omega/a0)
    b.append(1)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha)/a0)

    return b, a


def compute_peak_second_order(design_frequency_Hz, sampling_rate_Hz, gain_db, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section peak EQ filter
    Parameter: design_frequency_Hz: frequency where gain_dB gain is reached in Hertz
    sampling_rate_Hz: the sampling rate in Hertz
    gain_dB: the desired gain (+-) in dB
    Q: the Q factor, typical values are between 0.25 (peaky) to 4 (broad)
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)
    A = 10**(gain_db/40)
    cos_omega = np.cos(normalized_omega)
    a0 = 1+alpha/A
    b = []
    b.append((1+alpha*A)/a0)
    b.append(-2*cos_omega/a0)
    b.append((1-alpha*A)/a0)
    a = []
    a.append(1)
    a.append(-2*cos_omega/a0)
    a.append((1-alpha/A)/a0)

    return b, a


def compute_lowshelf_second_order(design_frequency_Hz, sampling_rate_Hz, gain_db, Q=1/np.sqrt(2)):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section shelfing (low) filter
    Parameter: design_frequency_Hz: frequency with gain_db/2 gain
    sampling_rate_Hz: the sampling rate in Hertz
    gain_dB: the desired gain (+-) in dB at 0 Hz
    Q: the Q factor, Q = 1/sqrt(2) leads to a maximally flat design (default value)
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)
    A = 10**(gain_db/40)
    cos_omega = np.cos(normalized_omega)
    two_sqrtA_alpha = 2*np.sqrt(A)*alpha

    a0 = (A+1) + (A-1)*cos_omega + two_sqrtA_alpha
    b = []
    b.append((A*((A+1) - (A-1)*cos_omega + two_sqrtA_alpha))/a0)
    b.append((2*A*((A-1) - (A+1)*cos_omega))/a0)
    b.append((A*((A+1) - (A-1)*cos_omega - two_sqrtA_alpha))/a0)
    a = []
    a.append(1)
    a.append(-2*((A-1) + (A+1)*cos_omega)/a0)
    a.append(((A+1) + (A-1)*cos_omega - two_sqrtA_alpha)/a0)

    return b, a


def compute_highshelf_second_order(design_frequency_Hz, sampling_rate_Hz, gain_db, Q=1/np.sqrt(2)):
    '''computes the coefficient b (transversal) and a (recursive) of a second order section shelfing (high) filter
    Parameter: design_frequency_Hz: frequency with gain_db/2 gain
    sampling_rate_Hz: the sampling rate in Hertz
    gain_dB: the desired gain (+-) in dB at fs/2 Hz
    Q: the Q factor, Q = 1/sqrt(2) leads to a maximally flat design (default value)
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)
    alpha = compute_alpha(normalized_omega=normalized_omega, Q=Q)
    A = 10**(gain_db/40)
    cos_omega = np.cos(normalized_omega)
    two_sqrtA_alpha = 2*np.sqrt(A)*alpha

    a0 = (A+1) - (A-1)*cos_omega + two_sqrtA_alpha
    b = []
    b.append((A*((A+1) + (A-1)*cos_omega + two_sqrtA_alpha))/a0)
    b.append((-2*A*((A-1) + (A+1)*cos_omega))/a0)
    b.append((A*((A+1) + (A-1)*cos_omega - two_sqrtA_alpha))/a0)
    a = []
    a.append(1)
    a.append(2*((A-1) - (A+1)*cos_omega)/a0)
    a.append(((A+1) - (A-1)*cos_omega - two_sqrtA_alpha)/a0)

    return b, a

# This design is based on
# Orfanidis, Sophoncles J., "Digital Parametric Equalizer Design with Prescribed Nyquist-Frequency Gain",
# JAES Volume 45 Issue 6 pp. 444-455; June 1997, https://www.aes.org/e-lib/browse.cfm?elib=13397


def compute_peak_orfan(design_frequency_Hz, sampling_rate_Hz, gain_db, Q):
    '''computes the coefficient b (transversal) and a (recursive) of a second order peak EQ filter
    In contrast to compute_peak_second_order the shape of the bell curve and its gain is correct at fs/2
    Parameter: design_frequency_Hz: frequency of maximum or minimum gain 
    sampling_rate_Hz: the sampling rate in Hertz
    gain_dB: the desired gain (+-) in dB at design_frequency_Hz
    Q: the Q factor, Q
    returns b,a : the coefficients that can be used by scipy.signal.lfilter
    '''

    G0 = 1
    G = 10**(gain_db/20)
    normalized_omega = compute_normalized_omega(
        design_frequency_Hz=design_frequency_Hz, sampling_rate_Hz=sampling_rate_Hz)

    GB = np.sqrt(G0*G)  # geometric mean
    #bw_Oct = Q_to_bandwidth(Q,normalized_omega)
    bw_Oct = Q_to_bandwidth_analog(Q)
    f_m = design_frequency_Hz/(np.sqrt(2**(bw_Oct)))
    f_p = 2**(bw_Oct)*f_m
    bw = f_p-f_m
    Dw = 2*np.pi*bw/sampling_rate_Hz

    #Dw = (2*np.pi*design_frequency_Hz/sampling_rate_Hz)/Q

    F = np.abs(G**2-GB**2)
    G00 = np.abs(G**2-G0**2)
    F00 = np.abs(GB**2-G0**2)

    num = G0**2*(normalized_omega**2-np.pi**2)**2+G**2*F00*np.pi**2*Dw**2/F
    den = (normalized_omega**2-np.pi**2)**2+F00*np.pi**2*Dw**2/F

    G1 = np.sqrt(num/den)

    G01 = np.abs(G**2-G0*G1)
    G11 = np.abs(G**2-G1**2)
    F01 = np.abs(GB**2-G0*G1)
    F11 = np.abs(GB**2-G1**2)

    W2 = np.sqrt(G11/G00)*np.tan(normalized_omega/2)**2
    DW = (1+np.sqrt(F00/F11)*W2)*np.tan(Dw/2)
    #DW = 2*F00/F11*sinh(w0/(sin(w0))*log(2)/2*bw_Oct);

    C = F11*DW**2-2*W2*(F01-np.sqrt(F00*F11))
    D = 2*W2*(G01-np.sqrt(G00*G11))

    A = np.sqrt((C+D)/F)
    B = np.sqrt((G**2*C + GB**2*D)/F)

    a0 = (1+W2+A)
    b = []
    b.append((G1+G0*W2+B)/a0)
    b.append(-2*(G1-G0*W2)/a0)
    b.append(((G1-B+G0*W2))/a0)
    a = []
    a.append(1)
    a.append((-2*(1-W2))/a0)
    a.append(((1+W2-A))/a0)
    return b, a


if __name__ == '__main__':

    f0 = 12000
    fs = 32000
    #Q = 1/np.sqrt(2)
    #Q = 2/np.sqrt(2)
    #Q = 10
    Q = 1
    gain = -10

    order = 2
    C = 1
    d, c = compute_analog_butterworth(order, C=C)

    b_butt, a_butt = sig.butter(2, 2*f0/fs)
    w, H = sig.freqz(b_butt, a_butt, fs=fs)

    #b_eq, a_eq  = compute_allpass_second_order(f0,fs,Q)
    b_eq, a_eq = compute_peak_orfan(f0, fs, gain, Q)
    w_eq, H_eq = sig.freqz(b_eq, a_eq, fs=fs)

    fig, ax = plt.subplots()

    # ax.plot(w, 20*np.log10(np.abs(H)))
    ax.plot(np.log(w_eq), 20*np.log10(np.abs(H_eq)))

    plt.show()
