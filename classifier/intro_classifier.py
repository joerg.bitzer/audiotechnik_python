import numpy as np
import matplotlib.pyplot as plt

import librosa as lr


speech_file_name = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/dev-clean/84/121123/84-121123-0000.flac'
noise_file_name = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/NOISEX/factory2.wav'

y_speech, sr = lr.load(speech_file_name, duration=4)

y_noise, sr = lr.load(noise_file_name, duration=4)


# was ist nicht gut an dem code?
rms_speech = lr.feature.rms(y_speech,frame_length=1024,hop_length=512)
rms_noise = lr.feature.rms(y_noise,frame_length=1024,hop_length=512)

nr_of_bins = 100
hist_speech, bin_speech = np.histogram(rms_speech, nr_of_bins)
hist_noise, bin_noise = np.histogram(rms_noise, nr_of_bins)

fig, ax = plt.subplots(nrows = 2)

ax[0].bar(bin_speech[:-1], hist_speech, width=0.02)
ax[1].bar(bin_noise[:-1], hist_noise, width=0.02)

plt.show()
# was ist problematisch an diesem Bild?








