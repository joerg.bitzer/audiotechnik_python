# this script shows a simple example how to use some scikit-learn functions to train and classify data
# (c) public domain. Question to J. Bitzer (at) Jade Hochschule (not an email)
# version 0.1 init build JB
# version 0.2 example work Dec 2022

import numpy as np
import matplotlib.pyplot as plt

import librosa as lr
import librosa.display as lrd

import scipy.stats as st

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import matthews_corrcoef
from sklearn.datasets import make_moons
from sklearn.svm import SVC
from sklearn import tree


file_name_speech = f'./speech_features_3_files.npy'
file_name_noise = f'./noise_features_3_files.npy'

all_feat_class1 = np.load(file_name_speech)
all_feat_class2 = np.load(file_name_noise)

# check if data are loaded correctly
#fig, ax = plt.subplots(nrows = 2)

#lrd.specshow(all_feat_class1,ax=ax[0])
#lrd.specshow(all_feat_class2,ax=ax[1])

#plt.show()

classes_are_balanced = True
randomize_features = True

len_class1 = all_feat_class1.shape[1]
len_class2 = all_feat_class2.shape[1]

if randomize_features:
    random_vec = np.random.permutation(len_class1)
    all_feat_class1 = all_feat_class1[:,random_vec]
    random_vec = np.random.permutation(len_class2)
    all_feat_class2 = all_feat_class2[:,random_vec]

if classes_are_balanced:
    if len_class1>len_class2:
        len_class1 = len_class2
        all_feat_class1 = all_feat_class1[:,:len_class1]
    elif len_class1 < len_class2:
        len_class2 = len_class1
        all_feat_class2 = all_feat_class2[:,:len_class2]

#print(len_class1)
#print(len_class2)


# labelvector für die zwei KLassen vorbereiten 
labelvec = np.zeros(len_class1+len_class2)
labelvec[:len_class1] = 1

# data connecten
all_data = np.hstack((all_feat_class1,all_feat_class2))

# randomizen und splitten 
train_data, test_data, train_label, test_label = train_test_split(all_data.transpose(), labelvec, train_size = 0.7, random_state=42)

# debug
#fig, ax = plt.subplots(nrows = 2)

#lrd.specshow(train_data,ax=ax[0])
#lrd.specshow(test_data,ax=ax[1])

#plt.show()



# training

#clf = tree.DecisionTreeClassifier(max_depth = 10) # try 3..5
clf = SVC(kernel='rbf', gamma = 0.001, C = 10)

clf = clf.fit(train_data, train_label)

# test 

predicted_class = clf.predict(test_data)

# evaluationsergebnisse anzeigen
mcc =  matthews_corrcoef(test_label, predicted_class)
conf_matrix = confusion_matrix(test_label, predicted_class)

print(mcc)
print(conf_matrix)




