# this script shows how to generate trainings and testdata for a classification framework
# (c) public domain. Question to J. Bitzer (at) Jade Hochschule (not an email)
# version 0.1 init build JB
# version 0.2 data can be generated

import numpy as np
import matplotlib.pyplot as plt

import librosa as lr
import librosa.display as lrd


def compute_and_append_features(old_feat_data, new_audio_data, block_len = 1024, hop_len = 512):
    mfcc = lr.feature.mfcc(y = new_audio_data, n_fft = block_len, hop_length = hop_len)
    mfcc_order = 1 # only delta
    mfcc_width = 3 # use only the innermost values
    delta_mfcc = lr.feature.delta(mfcc, width = mfcc_width, order = mfcc_order)

    feat_vec = np.vstack((mfcc,delta_mfcc))
    if len(old_feat_data) == 0:
        return feat_vec
    else:
        return np.hstack((old_feat_data, feat_vec))


def xtract_n_files(file_list, nr_of_files, random_flag = False):
    
    all_feat_class = []
    filelist_len = len(file_list)
    if nr_of_files > filelist_len:
        nr_of_files = len(filelist_len)
    
    if random_flag:
        rand_files = np.random.permutation(filelist_len)
        filenr_list = rand_files[:nr_of_files]
    else:
        filenr_list = range (nr_of_files)



    for kk in range(nr_of_files):
        y, sr = lr.load(file_list[filenr_list[kk]])
        all_feat_class = compute_and_append_features(all_feat_class,y)
        if kk == nr_of_files:
            break
    
    return all_feat_class


base_path = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/'
class1_path = 'dev-clean'
class2_path = 'NOISEX'

# settings
nr_of_class1_files = 3  #  how many files will be computed
nr_of_class2_files = 3  #  how many files will be computed
choose_files_randomly = True

featfile_name_speech = f'speech_features_{nr_of_class1_files}_files'
featfile_name_noise = f'noise_features_{nr_of_class2_files}_files'


# program
all_class1_files = lr.util.find_files(base_path+class1_path,ext='flac', recurse=True)
all_class2_files = lr.util.find_files(base_path+class2_path,ext='wav', recurse=True)

all_feat_class1 = xtract_n_files(all_class1_files, nr_of_class1_files, random_flag=choose_files_randomly)
all_feat_class2 = xtract_n_files(all_class2_files, nr_of_class2_files, random_flag=choose_files_randomly)

np.save(featfile_name_speech,all_feat_class1)
np.save(featfile_name_noise,all_feat_class2)

#debug
#fig, ax = plt.subplots(nrows = 2)

#lrd.specshow(all_feat_class1,ax=ax[0])
#lrd.specshow(all_feat_class2,ax=ax[1])

#plt.show()








