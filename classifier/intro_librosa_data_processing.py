import numpy as np
import matplotlib.pyplot as plt

import librosa as lr
import librosa.display as lrd

import scipy.stats as st

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import matthews_corrcoef
from sklearn.datasets import make_moons
from sklearn.svm import SVC
from sklearn import tree


base_path = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/'
class1_path = 'dev-clean'
class2_path = 'NOISEX'

all_class1_files = lr.util.find_files(base_path+class1_path,ext='flac', recurse=True)
all_class2_files = lr.util.find_files(base_path+class2_path,ext='wav', recurse=True)


def compute_and_append_features(old_feat_data, new_audio_data, block_len = 1024, hop_len = 512):
    mfcc = lr.feature.mfcc(y = new_audio_data, n_fft = block_len, hop_length = hop_len)
    mfcc_order = 1 # only delta
    mfcc_width = 3 # use only the innermost values
    delta_mfcc = lr.feature.delta(mfcc, width = mfcc_width, order = mfcc_order)

    feat_vec = np.vstack((mfcc,delta_mfcc))
    if len(old_feat_data) == 0:
        return feat_vec
    else:
        return np.hstack((old_feat_data, feat_vec))

    

all_feat_class1 = []
all_feat_class2 = []

speech_file_name = all_class1_files[0]
noise_file_name = all_class2_files[0]

y_speech, sr = lr.load(speech_file_name)
y_noise, sr = lr.load(noise_file_name)

all_feat_class1 = compute_and_append_features(all_feat_class1,y_speech)
all_feat_class2 = compute_and_append_features(all_feat_class2,y_speech)

all_feat_class1 = compute_and_append_features(all_feat_class1,y_speech)
all_feat_class2 = compute_and_append_features(all_feat_class2,y_speech)

#mfcc_speech = lr.feature.mfcc(y = y_speech, n_fft = 1024, hop_length =512)
#mfcc_noise = lr.feature.mfcc(y = y_noise, n_fft = 1024, hop_length =512)

#delta_mfcc_speech = lr.feature.delta(mfcc_speech,width=3,order=1)
#delta_mfcc_noise = lr.feature.delta(mfcc_noise,width=3,order=1)

# perhaps normalize data (something to be tested)

#mfcc_speech = st.zscore(mfcc_speech,axis = 1)
#mfcc_noise = st.zscore(mfcc_noise,axis = 1)


#combine  all  feature

#feat_vec_speech = np.vstack((mfcc_speech,delta_mfcc_speech))
#feat_vec_noise = np.vstack((mfcc_noise,delta_mfcc_noise))

#all_feat_class1 = np.hstack((all_feat_class1,delta_mfcc_speech))
#all_feat_class2 = np.hstack((all_feat_class2,delta_mfcc_speech))




fig, ax = plt.subplots(nrows = 2)

lrd.specshow(all_feat_class1,ax=ax[0])
lrd.specshow(all_feat_class2,ax=ax[1])


plt.show()
# was ist problematisch an diesem Bild?








