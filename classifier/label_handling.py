import numpy as np

import matplotlib.pyplot as plt
import soundfile as sf
import pathlib

import os

def is_label_available(file_name):
    file_exist = os.path.exists(file_name)
    if not file_exist:
        return -1
    
    file_base,file_type = os.path.splitext(file_name)
    return (os.path.exists(file_base + '.txt'))

def read_label_for_audiofile(file_name):
    has_label = is_label_available(file_name)
    if has_label is not True:
        return None
    Data = None
    if has_label:
        file_base,file_type = os.path.splitext(file_name)
        Data = np.genfromtxt(file_base+'.txt', dtype=[('start', 'f',),('stop','f'),('label','S20')], delimiter = '\t', autostrip=True)
        #Data = np.genfromtxt(file_base+'.txt', delimiter = '\t', autostrip=True)

    return Data

def get_all_unique_label_names(label_data):
    label_name = set()
    for onelabel in label_data:
        cur_label_name = onelabel[2].decode() # decode bytecode to string
        label_name.add(cur_label_name)
    
    return list(label_name)

def cut_audiofile_at_label(file_name, label_name):
    label_data = read_label_for_audiofile(file_name)
    if label_data is None:
        return None
    
    audio_data, fs = sf.read(file_name)

    cut_data = []
    for onelabel in label_data:
        start_sample = int(onelabel[0]*fs+0.5) # +0.5 == round
        end_sample = int(onelabel[1]*fs+0.5) # +0.5 == round
        cur_label_name = onelabel[2].decode() # decode bytecode to string
        
        if str(cur_label_name) == label_name:
            if audio_data.ndim == 1:
                onecut = audio_data[start_sample:end_sample]
            else: 
                onecut = audio_data[start_sample:end_sample,:]

            cut_data.append(onecut)
        
    return cut_data, fs


file_name = '/home/bitzer/Lehre/Audiotechnik/Abschlussarbeiten/22_23/BitzerBell2.wav'

label_data = read_label_for_audiofile(file_name)

uniques_labels = get_all_unique_label_names(label_data)

cutted_data, fs = cut_audiofile_at_label(file_name,uniques_labels[0])

main_path, basename = os.path.split(file_name)

outpath = os.path.join(main_path, uniques_labels[0])

os.makedirs(outpath, exist_ok=True)

for kk, onefile in enumerate(cutted_data):
    name, suffix = os.path.splitext(basename)
    filename = os.path.join(outpath,f'{name}_{kk}.{suffix}')
    
    sf.write(filename,onefile,fs)









#D = np.genfromtxt(file_name, dtype=[('start', 'f',),('stop','f'),('label','S20')], delimiter = '\t', autostrip=True)

# Get labels
#labels = set([ann[2] for ann in D])