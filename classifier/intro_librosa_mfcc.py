import numpy as np
import matplotlib.pyplot as plt

import librosa as lr
import librosa.display as lrd

import scipy.stats as st

speech_file_name = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/dev-clean/84/121123/84-121123-0000.flac'
noise_file_name = '/home/bitzer/Lehre/Audiotechnik/audiotechnik_python/classifier/NOISEX/factory2.wav'

y_speech, sr = lr.load(speech_file_name, duration=4)
y_noise, sr = lr.load(noise_file_name, duration=4)


# was ist nicht gut an dem code?
mfcc_speech = lr.feature.mfcc(y = y_speech, n_fft = 1024, hop_length =512)
mfcc_noise = lr.feature.mfcc(y = y_noise)

# interesting, but not allowed 
#mfcc_speech = st.zscore(mfcc_speech,axis = 1)
#mfcc_noise = st.zscore(mfcc_noise,axis = 1)

fig, ax = plt.subplots(nrows = 2)

lrd.specshow(mfcc_speech,ax=ax[0])
lrd.specshow(mfcc_noise,ax=ax[1])


plt.show()
# was ist problematisch an diesem Bild?








